import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HevolucionCgComponent } from './hevolucion-cg.component';

describe('HevolucionCgComponent', () => {
  let component: HevolucionCgComponent;
  let fixture: ComponentFixture<HevolucionCgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HevolucionCgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HevolucionCgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
