import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficasPediatriaComponent } from './graficas-pediatria.component';

describe('GraficasPediatriaComponent', () => {
  let component: GraficasPediatriaComponent;
  let fixture: ComponentFixture<GraficasPediatriaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraficasPediatriaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficasPediatriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
