import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaInfoUserComponent } from './ficha-info-user.component';

describe('FichaInfoUserComponent', () => {
  let component: FichaInfoUserComponent;
  let fixture: ComponentFixture<FichaInfoUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FichaInfoUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaInfoUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
