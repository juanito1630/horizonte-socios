import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { LocationServiceService } from 'src/app/services/otrosService/location-service.service';
import { PacientesService } from 'src/app/services/pacientes/pacientes.service';
import swal from 'sweetalert'
import {  MatStepper  } from '@angular/material/stepper';
import { splitAtColon } from '@angular/compiler/src/util';
// import { NgStepperModule } from 'angular-ng-stepper';


@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css']
})
export class StepperComponent implements OnInit {

  public estados: [];
  public paises:[];
  public municipios:[];
  public estado: string;
  public paquetes: [];
  public validateBtn = false;
  public edadPaciente: HTMLInputElement;

  
  constructor(
    public _locationService: LocationServiceService,
    public _pacienteService: PacientesService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.obtenerEstados();
    // this.obtenerPaquetes();
    this.obtenerPaises();
  }

  obtenerPaises(){

    this._locationService.getCountries()
    .subscribe(
      (data:any) => {  this.paises = data;});
  }

  obtenerEstados(){
    this._locationService.getEstado( )
    .subscribe((data:any) => {
        this.estados = data.response['estado']
      });
  }

  ObtenerMunicipio(estado){
    this._locationService.getMunicipios(estado)
    .subscribe(
      (data:any) => {
        this.municipios = data['response']['municipios']
      });
  }

  buscarMunicipios() {
    this.ObtenerMunicipio( this.estado )
  }


  validarSexo( sexo: string ){
    console.log( sexo );
    if(  sexo.length < 4 || sexo == "" ){
      return false;
    }else{
      return true;
    }
  }

  validarEdad(edad:number){
    if( edad > 110  ){
      return false;
    }else{
      return true;
    }
  }

  generarEdad(edadForm: HTMLInputElement ) {
    this.edadPaciente = document.querySelector('#edad');

    // console.log( edadForm.value );
    let fecha = edadForm.value;
    let splitFecha = fecha.split('-');
    var fechaActual = new Date();
    var anio = fechaActual.getFullYear();

    let edadNormal = anio - parseFloat( splitFecha[0]  );
    let edades = edadNormal.toString();
    this.edadPaciente.value = edades;
    
    


  }

  validarCurp( curp: string  ){
    if(  curp.length == 18  ){
      return true;
    }else{
      return false;
    }
  }

  validarString(nombre: string){
    if(nombre == ""  || nombre.length <3 ){
        return false;
    }
    return true;
  }

  message(msj){
    swal(msj,{icon:"success"})
    /* alert(msj); */
  }

  validacones(form){

    if( this.validarString(form.nombrePaciente) ){
    
    }else {
      this.message('Completa el nombre')
      return false;
    }

    if(  this.validarString(form.apellidoPaterno) ){
     }else {
       this.message('Completa el apellido')
       return false;
     }
     if( this.validarString(form.apellidoMaterno) ){

      }else{
        this.message('Completa el apellido')
        return false;
      }

     if( this.validarCurp( form.curp  )   ){
     }else{
       this.message('Ingresa un curp valido')
       return false;
     }

     if(this.validarEdad(form.edad)  ){

     }else{
       this.message('Ingrea una edad valida')
       return false;
     }

     if ( this.validarSexo(form.genero) ){

     }else{
        this.message('Ingresa el sexo del paciente');
      return false;
     }

    //  this.validateBtn = true;
    return true;

  }

  enviar( form: NgForm  ){

    let resultado =this.validacones( form.value );
    // console.log( form.value );
    
    form.value.edad = this.edadPaciente.value
    // console.log( form.value );
    if(  resultado ){

      this.validateBtn = true;
      this._pacienteService.setPacientes(   form.value , 'CTLA01'  )
      .subscribe((data) => {
        
        if(  data['ok'] ){
          swal("Paciente registrado",{icon:"success"})
          /* alert('Paciente registrado'); */
          this._router.navigateByUrl('/paciente')
      
        }
      });

    }else {

      return;

    }
  
  }

}
